Transmon
========

Transmon (Translation Monitor) is a translation monitor.  It uses a manifest
that lists projects you are interested in.  It will get the latest release or
development version (e.g. latest git commit) and try to find localisation
information in the project.

The result of the tool is a json representation of the localisation state of the
different projects.  The output is a list of projects.  Each project has attributes
"name", "home-page" and "state".  The state attribute contains an object whose
attributes are the name of languages (en, fr, de, …) and values are a couple
composed of the number of translated strings in that language and the number of
available strings in the project.

Example
-------

projects.manifest:

```json
[
  {
    "name": "dns66",
    "variants": [
      {
        "fetcher": "git",
        "uri": "https://github.com/julian-klode/dns66",
        "tags": ["master"],
	"name": "git-master"
      }
    ],
    "tags": ["android", "fdroid"],
    "home-page": "https://github.com/julian-klode/dns66"
  }
]

```

Then, run:

```bash
$ transmon projects.manifest
```

Alternatively, if you don't want to save a cache of fetched repositories
(to save bandwidth latter), you can run:

```bash
$ transmon --temp projects.manifest
```

Finally, you will get the result on the standard output:

```json
[{"name": "dns66",
  "home-page": "https://github.com/julian-klode/dns66",
  "state": [
    {"filemask": "app/src/main/res/values-*/strings.xml",
     "template": "app/src/main/res/values/strings.xml",
     "file_format": "aresource",
     "state": {
       "nl": [94, 102], "tr": [93, 102], "zh": [92, 102], "et": [94, 102],
       "ru": [92, 102], "it": [94, 102], "ja": [93, 102], "pl": [93, 102],
       "fr": [102, 102], "es": [94, 102], "nb": [94, 102], "de": [102, 102]}}
  ]}
]
```


