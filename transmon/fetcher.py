#   Copyright (c) 2018 Julien Lepiller <julien@lepiller.eu>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as
#   published by the Free Software Foundation, either version 3 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
####
import tempfile
import pathlib
import pygit2
import shutil
import sys

class Fetcher(object):
    def __init__(self):
        self.path = None
        return

    def fetch(self, project):
        raise Exception('Not implemented')

    def fetchIn(self, project, path):
        raise Exception('Not implemented')

    def cleanup(self):
        if self.path is None:
            return
        self.path.cleanup()
        self.path = None

    def getDirectory(self):
        if self.path == None:
            return None
        if isinstance(self.path, pathlib.Path):
            return str(self.path)
        else:
            return self.path.name


class Progress(pygit2.remote.RemoteCallbacks):
    def transfer_progress(self, stats):
        print("{}/{}\r".format(stats.received_objects, stats.total_objects), file=sys.stderr, end='')

class GitFetcher(Fetcher):
    def __init__(self):
        Fetcher.__init__(self)

    def fetch(self, uri):
        path = tempfile.TemporaryDirectory()
        self.path = path
        pygit2.clone_repository(uri, path.name, callbacks=Progress())
        return path.name

    def pull(self, repo):
        for remote in repo.remotes:
            remote.fetch()
        remote_ref = "refs/remotes/origin/" + repo.head.name.split('/')[-1]
        remote_ref = repo.references.get(remote_ref)

        remote_master_id = remote_ref.target
        repo.checkout_tree(repo.get(remote_master_id))
        repo.head.set_target(remote_master_id)

    def fetchIn(self, uri, path):
        self.path = pathlib.Path(path)
        self.path.mkdir(parents=True, exist_ok=True)
        try:
            repo = pygit2.Repository(path)
            self.pull(repo)
        except:
            shutil.rmtree(path)
            pygit2.clone_repository(uri, path, callbacks=Progress())
        return path
