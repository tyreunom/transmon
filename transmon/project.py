#   Copyright (c) 2018 Julien Lepiller <julien@lepiller.eu>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as
#   published by the Free Software Foundation, either version 3 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
####
from pathlib import Path
import sys

class Project(object):
    def __init__(self, name, variant, fetcher, uri, homepage):
        self.name = name
        self.variant = variant
        self.fetcher = fetcher
        self.uri = uri
        self.homepage = homepage
        self.dir = None

    def set_analyzer(self, analyzer):
        self.analyzer = analyzer

    def fetch(self, is_temp):
        print("Fetching " + self.name + " from " + self.uri + " ...", file=sys.stderr)
        if is_temp:
            return self.fetcher.fetch(self.uri)
        else:
            return self.fetcher.fetchIn(self.uri, str(Path.home()) + '/.cache/transmon/' + self.name + '/' + self.variant)

    def analyze(self):
        print("Analyzing " + self.name + "...", file=sys.stderr)
        return self.analyzer.analyze(self.fetcher.getDirectory())

    def cleanup(self):
        self.fetcher.cleanup()
