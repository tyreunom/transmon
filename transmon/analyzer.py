#   Copyright (c) 2018 Julien Lepiller <julien@lepiller.eu>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as
#   published by the Free Software Foundation, either version 3 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
####
import os
import polib
import sys
from pathlib import Path
from androidstringslib import android

class Analyzer(object):
    def __init__(self):
        return

    def analyze(self, path):
        raise Exception('Not implemented')

class AndroidAnalyzer(Analyzer):
    def __init__(self, templatepath):
        self.templatepath = templatepath
        Analyzer.__init__(self)

    def analyze(self, path):
        respath = os.path.dirname(os.path.dirname(path + '/' + self.templatepath))
        dirs = os.listdir(respath)
        dirs = [(respath + "/" + x) for x in dirs
                    if (os.path.isdir(respath + "/" + x) and
                        'strings.xml' in os.listdir(respath + "/" + x))]
        res = {}
        for p in dirs:
            if p.endswith('values'):
                continue
            lang = p[p.find('/values-')+8:]
            try:
                an = android(respath + '/values/strings.xml', p + '/strings.xml',
                             lang)
            except:
                print('Skipping {} because of errors.'.format(lang),
                    file=sys.stderr)
                continue
            translated = 0
            number = len(an)
            for entry in an:
                if entry.dst != '' and not entry.fuzzy:
                    translated = translated + 1
            res[lang] = (translated, number)
        return res

class AppstoreAnalyzer(Analyzer):
    def __init__(self, template, translation):
        self.template = template
        self.translation = translation
        Analyzer.__init__(self)

    def analyze(self, path):
        res = {}
        appstore = Path(path + '/' + self.template).parent
        template = Path(path + '/' + self.template)
        template_files = [x for x in list(template.glob('**/*')) if x.is_file()]
        template_files = [str(x)[len(str(template))+1:] for x in template_files]
        langs = appstore.glob('*')
        for lang in langs:
            if not lang.is_dir():
                continue
            if str(lang) == str(template):
                continue
            langname = str(lang)[len(str(appstore))+1:]
            lang_files = [x for x in list(lang.glob('**/*')) if x.is_file()]
            lang_files = [str(x)[len(str(lang))+1:] for x in lang_files]
            lang_files = [x for x in lang_files if x in template_files]
            res[langname] = (len(lang_files), len(template_files))
        return res

class GettextAnalyzer(Analyzer):
    def __init__(self, pot, pofiles, mono=False):
        self.pot = pot
        self.pofiles = pofiles
        self.mono = mono
        Analyzer.__init__(self)

    def analyze(self, path):
        if self.mono:
            respath = os.path.dirname(os.path.dirname(path + "/" + self.pofiles))
        else:
            respath = os.path.dirname(path + "/" + self.pofiles)
        # pofiles = po/manual/guix-manual.*.po
        # pofiles = po/*/app.po
        dirs = os.listdir(respath)
        if self.mono:
            poname = os.basename(path + "/" + self.pofiles)
            dirs2 = [os.listdir(p) for p in dirs]
            pofiles = [p for p in dirs2 if (os.basename(path + "/" + p)) == poname]
        else:
            pofiles = [p for p in dirs if p[-2:] == "po"]
        res = {}
        for pofile in pofiles:
            po = polib.pofile(respath + "/" + pofile)
            if self.pot is not None:
                pot = polib.pofile(self.pot)
                po.merge(pot)
            translated = 0
            number = 0
            for entry in po:
                if entry.obsolete == 1:
                    continue
                number = number + 1
                if entry.msgstr != "":
                    translated = translated + 1
            res[po.metadata['Language']] = (translated, number)
        return res
