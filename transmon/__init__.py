#   Copyright (c) 2018 Julien Lepiller <julien@lepiller.eu>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as
#   published by the Free Software Foundation, either version 3 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
####
import sys
import json

from translation_finder import discover

from .project import Project
from .fetcher import GitFetcher
from .analyzer import AndroidAnalyzer
from .analyzer import AppstoreAnalyzer
from .analyzer import GettextAnalyzer

def main():
    if(len(sys.argv) < 2):
        print('Usage: ' + sys.argv[0] + ' [--temp] projects-file')
        print('  --temp: Use temporary files to store repos.')
        sys.exit(1)
    temp = False
    projfile = sys.argv[1]
    if sys.argv[1] == "--temp":
        temp = True
        projfile = sys.argv[2]
    with open(projfile, encoding='utf-8') as f:
        myconf = json.load(f)
        conf = []
        for proj in myconf:
            for variant in proj['variants']:
                v = variant
                if 'tags' in v:
                    v['tags'].append(proj['tags'])
                else:
                    v['tags'] = proj['tags']
                v['home-page'] = proj['home-page']
                v['variant'] = v['name']
                v['name'] = proj['name']
                conf.append(v)
        res = []
        i=1
        for proj in conf:
            try:
                print("Project {} / {}".format(i, len(conf)), file=sys.stderr)
                i+=1
                if proj['fetcher'] == 'git':
                    f = GitFetcher()
                else:
                    raise Exception('No such fetcher: ' + proj['fetcher'])
                p = Project(proj['name'], proj['variant'], f, proj['uri'], proj['home-page'])
                path = p.fetch(temp)
                translations = discover(path)
                state = []
                for resource in translations:
                    if resource['file_format'] == 'aresource':
                        a = AndroidAnalyzer(resource['template'])
                    elif resource['file_format'] == 'po':
                        a = GettextAnalyzer(resource['new_base'] if hasattr(resource, 'new_base') else None, resource['filemask'])
                    elif resource['file_format'] == 'po-mono':
                        a = GettextAnalyzer(resource['new_base'], resource['filemask'], True)
                    elif resource['file_format'] == 'appstore':
                        a = AppstoreAnalyzer(resource['template'], resource['filemask'])
                    else:
                        raise Exception('No such analyzer: ' + resource['file_format'])
                    p.set_analyzer(a)
                    resource['state'] = p.analyze()
                    state.append(resource)
                res.append({"name": proj['name'], 'home-page': proj['home-page'],
                    'state': state})
                if temp:
                    p.cleanup()
            except Exception as e:
                print("Exception while analyzing " + proj['name'] + '/' + \
                        proj['variant'] + ': ' + str(e), file=sys.stderr)
        print(json.dumps(res))
